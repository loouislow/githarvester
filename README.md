# gitHarvest(er)
> A real-time repository committing toolkit for GIT
---
## Prerequisites

You need these packages to be installed.

- git
- inotifywait

Run `gitinit.sh` each time you create a new repository will also help to check if these packages are installed.
```
$ ./gitinit.sh
```
---

## Files

**gitinit.sh** -- script to create new repository

**gitcogs.sh** -- script to set up the watchdog

**githarvest.sh** -- script that do all the magic

> **Note:** Recommended to symlink the scripts to `/usr/local/bin` and `chmod +x *` for easy fiddling with. And also rename/remove file extension `.sh`; for example `gitinit.sh` renamed to just `gitinit`.

## Optional

> **Setup** with `cronjob`.
---

## Usage:

> **Type:** githarvest 
${0##*/} [-s <secs>] [-d <fmt>] [-r <remote> [-b <branch>]] [-m <msg>] <target>

Where is the file or folder which should be watched. The target needs to be in a Git repository, or in the case of a folder, it may also be the top folder of the repo.

## -s <secs>

After detecting a change to the watched file or directory, wait seconds until committing, to allow for more write actions of the same batch to finish; default is 2 sec.

## -d <fmt>
 
The format string used for the time-stamp in the commit. message; see 'man date' for details; default is `"+%Y-%m-%d %H:%M:%S"`.

## -r <remote>

If defined, a *`git push`* to the given is done after every commit.

## -b <branch>
 
The branch which should be pushed automatically;
> If not given, the push command used is 'git push ', thus doing a default push (see git man pages for details).

> If given and + repo is in a detached HEAD state (at launch) then the command used is 'git push ' + repo is NOT in a detached HEAD state (at launch) then the command used is git push :' where is the target of HEAD (at launch).

> if no remote was define with *`-r`*, this option has no effect.

## -m <msg>

The commit message used for each commit;

> All occurrences of %d in the string will be replaced by the formatted date/time (unless the specified by *`-d`* is empty, in which case %d is replaced by an empty string);

> The default message is: "`Auto-commit on change (%d) by gitHarvest`"

---

As indicated, several conditions are only checked once at launch of the script. You can make changes to the repo state and configurations even while the script is running, but that may lead to undefined and unpredictable (even destructive) behavior!

It is therefore recommended to terminate the script before changing the repo's config and restarting it afterwards.

By default, gitHarvest tries to use the binaries "git" and "inotifywait", expecting to find them in the PATH (it uses 'which' to check this and will abort with an error if they cannot be found). If you want to use binaries that are named differently and/or located outside of your PATH, you can define replacements in the environment variables GW_GIT_BIN and GW_INW_BIN for git and inotifywait, respectively.
