#!/bin/bash
#
# @@script: gitinit.sh
# @@description: create new git repository
# @@author: Loouis Low
# @@copyright: GNU GPL
#

### run as root
function runas_root {
   if [ "$(whoami &2>/dev/null)" != "root" ] && [ "$(id -un &2>/dev/null)" != "root" ]
     then
       echo "[gitinit] Permission denied."
     exit 1
   fi
}

### check required packages
function check_prerequisites {
   if ! [ -x "$(command -v git)" ]; then
      echo "[gitinit] Installing required packages..." >&2
      sudo apt-get install -y inotify-tools git
   fi
}

### create new repository
function new_git_init {
   echo "[gitinit] Create new git..."
   git init
   
   echo "[gitinit] Commit new changes..."
   git add .
   
   echo "[gitinit] OK. You should set up (gitHarvest) after this."
}

### initialize
#runas_root
check_prerequisites
new_git_init
