#!/bin/bash
#
# @@script: gitcogs.sh
# @@description: Example watchdog settings
# @@author: Loouis Low
# @@copyright: GNU GPL
#

### constants
export USER='loouis'
export TERM='xterm-256color'
export HOME="/home/${USER}"
WATCH_DIR_1="${HOME}/Documents/folder1"
WATCH_DIR_2="${HOME}/Documents/folder2"
WATCH_DIR_3="${HOME}/Documents/folder3"
WATCH_DIR_4="${HOME}/Documents/folder4"
WATCH_DIR_1="${HOME}/Documents/folder5"
WATCH_TIME="5"

### watchdog
function watch_now {
   echo "[gitcogs] Batch running..."
   githarvest -s $WATCH_TIME $WATCH_DIR_1
   githarvest -s $WATCH_TIME $WATCH_DIR_2
   githarvest -s $WATCH_TIME $WATCH_DIR_3
   githarvest -s $WATCH_TIME $WATCH_DIR_4
   githarvest -s $WATCH_TIME $WATCH_DIR_5
}

### initialize
watch_now
